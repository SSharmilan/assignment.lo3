package OnlineBooking;
import java.util.Scanner;
public class BuyPage {
	private void checkOut(String productName,int price)
    {
        System.out.println("Select a method \n1.Visa\n2.Master");
        Scanner scanner = new Scanner(System.in);
        int ans = scanner.nextInt();
        showStatus(ans,productName,price);
    }

    private void showStatus(int ans, String productName, int price)
    {
        String status = null;
        if(ans ==1)
        {
            status = "Visa Card";
        }
        else if(ans ==2)
        {
            status = "Master Card";
        }
        else
        {
            System.out.println("Enter correct number");
        }
        System.out.println("Product :" + productName + "\n" +
                "Total price :" + price + "\nPayment method :" + status);
    }

    public void runBuy(String productName, int price)
    {
        checkOut(productName, price);
    }
}
