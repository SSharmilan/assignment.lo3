package OnlineBooking;
import java.util.Scanner;

public class Product {
	private void Products()
    {
        System.out.println("\n 1.Books\n 2.Toys \n 3.Dresses \n");
        Scanner scanner = new Scanner(System.in);
        int ans = scanner.nextInt();
        productShowCase productShowCase = new productShowCase();
        switch (ans)
        {
            case 1:
                productShowCase.books();
                break;
            case 2:
                productShowCase.toys();
                break;
            case 3:
                productShowCase.dresses();
                break;
            default:
                System.out.println("Your answer is wrong");
                break;

        }
    }

    public void runProduct()
    {
        Products();
    }

}
